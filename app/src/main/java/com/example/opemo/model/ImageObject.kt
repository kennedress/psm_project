package com.example.opemo.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ImageObject(
    @SerializedName("Name")
    @Expose
    val name: String?,
    @SerializedName("Message")
    @Expose
    val message: String?
)
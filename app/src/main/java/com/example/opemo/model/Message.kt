package com.example.opemo.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Message (
    @SerializedName("Id")
    @Expose
    val id: String?,
    @SerializedName("IdUser")
    @Expose
    val idUser: String?,
    @SerializedName("IdObject")
    @Expose
    val idObject: String?,
    @SerializedName("Mensajes")
    @Expose
    var mensajes: String?,
    @SerializedName("Date")
    @Expose
    var date: String?,
    @SerializedName("User")
    @Expose
    var user: String?

)
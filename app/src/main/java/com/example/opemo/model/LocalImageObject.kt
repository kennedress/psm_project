package com.example.opemo.model

import android.graphics.Bitmap

class LocalImageObject(
    val id: String?,
    val name: String,
    val b64: String,
    val image: Bitmap?,
    val type: String
)
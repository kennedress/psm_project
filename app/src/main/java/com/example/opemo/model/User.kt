package com.example.opemo.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("Id")
    @Expose
    val id: String?,
    @SerializedName("Nick")
    @Expose
    val nick: String?,
    @SerializedName("Pass")
    @Expose
    val pass: String?,
    @SerializedName("Email")
    @Expose
    val email: String?,
    @SerializedName("Status")
    @Expose
    val status: String?,
    @SerializedName("Message")
    @Expose
    val message: String?
)
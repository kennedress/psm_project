package com.example.opemo.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Object(
    @SerializedName("Id")
    @Expose
    val id: String?,
    @SerializedName("IdUser")
    @Expose
    val idUser: String?,
    @SerializedName("Title")
    @Expose
    var title: String?,
    @SerializedName("Description")
    @Expose
    var description: String?,
    @SerializedName("Date")
    @Expose
    val date: String?,
    @SerializedName("Status")
    @Expose
    var status: String?,
    @SerializedName("Latitude")
    @Expose
    val latitude: String?,
    @SerializedName("Longitude")
    @Expose
    val longitude: String?,
    @SerializedName("Message")
    @Expose
    val message: String?
)
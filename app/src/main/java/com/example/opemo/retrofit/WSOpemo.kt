package com.example.opemo.retrofit

import com.example.opemo.model.ImageObject
import com.example.opemo.model.Message
import com.example.opemo.model.Object
import com.example.opemo.model.User
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.HashMap
import kotlin.jvm.internal.Intrinsics


class WSOpemo {
    companion object{
        private val WSOpemo: IWSOpemo by lazy {
            IWSOpemo.create()
        }

        fun getCUser(user: String, pass: String, email: String,
                     completation: (onFinish: User?, onString: String?, onError: String?) -> Unit) {

            val body = HashMap<String, String>()
            body["user"] = user
            body["pass"] = pass
            body["email"] = email
            val call = WSOpemo.createUser(body)
            call.enqueue(object : Callback<String>{
                override fun onFailure(call: Call<String>, t: Throwable) {
                    completation(null, null, t.localizedMessage)
                }

                override fun onResponse(call: Call<String>, response: Response<String>) {
                    response.let {
                        if(it.isSuccessful){
                            it.body()?.let { response ->
                                val obj = JSONObject(response)
                                val data = obj.getString("result")
                                val type = object : TypeToken<User?>() {}.type
                                val usuario = Gson().fromJson<User>(data, type)

                                completation(usuario, data, null)
                            }
                        }else{
                            completation(null, null, it.message())
                        }
                    }
                }
            })
        }

        fun login(user: String, pass: String,
                  completation: (onFinish: User?, onString: String?, onError: String?)-> Unit) {

            val body = HashMap<String, String>()
            body["user"] = user
            body["pass"] = pass

            val call = WSOpemo.readLogin(body)
            call.enqueue(object : Callback<String>{
                override fun onFailure(call: Call<String>, t: Throwable) {
                    completation(null, null, t.localizedMessage)
                }

                override fun onResponse(call: Call<String>, response: Response<String>) {
                    response.let {
                        if(it.isSuccessful){
                            it.body()?.let { response ->
                                val obj = JSONObject(response)
                                val data = obj.getString("result")
                                val type = object : TypeToken<User?>() {}.type
                                val usuario = Gson().fromJson<User>(data, type)

                                completation(usuario, data, null)
                            }
                        }else{
                            completation(null, null, it.message())
                        }
                    }
                }
            })
        }

        fun updateUser(id: String, nick: String, pass: String, email: String,
                       completation: (onFinish: User?, onString: String?, onError: String?)-> Unit) {

            val body = HashMap<String, String>()
            body["id"] = id
            body["user"] = nick
            body["pass"] = pass
            body["email"] = email

            val call = WSOpemo.updateUser(body)
            call.enqueue(object : Callback<String>{
                override fun onFailure(call: Call<String>, t: Throwable) {
                    completation(null, null, t.localizedMessage)
                }

                override fun onResponse(call: Call<String>, response: Response<String>) {
                    response.let {
                        if(it.isSuccessful){
                            it.body()?.let { response ->
                                val obj = JSONObject(response)
                                val data = obj.getString("result")
                                val type = object : TypeToken<User?>() {}.type
                                val usuario = Gson().fromJson<User>(data, type)

                                completation(usuario, data, null)
                            }
                        }else{
                            completation(null, null, it.message())
                        }
                    }
                }
            })
        }

        fun deleteUser(id: String,
                       completation: (onFinish: User?, onString: String?, onError: String?)-> Unit) {

            val body = HashMap<String, String>()
            body["id"] = id

            val call = WSOpemo.deleteUser(body)
            call.enqueue(object : Callback<String>{
                override fun onFailure(call: Call<String>, t: Throwable) {
                    completation(null, null, t.localizedMessage)
                }

                override fun onResponse(call: Call<String>, response: Response<String>) {
                    response.let {
                        if(it.isSuccessful){
                            it.body()?.let { response ->
                                val obj = JSONObject(response)
                                val data = obj.getString("result")
                                val type = object : TypeToken<User?>() {}.type
                                val usuario = Gson().fromJson<User>(data, type)

                                completation(usuario, data, null)
                            }
                        }else{
                            completation(null, null, it.message())
                        }
                    }
                }
            })
        }

        fun updateImageProfile(id: String, imgb64: String, type: String,
                               completation: (onFinish: User?, onString: String?, onError: String?)-> Unit) {

            val body = HashMap<String, String>()
            body["id"] = id
            body["imgb64"] = imgb64
            body["type"] = type

            val call = WSOpemo.updateImageProfile(body)
            call.enqueue(object : Callback<String>{
                override fun onFailure(call: Call<String>, t: Throwable) {
                    completation(null, null, t.localizedMessage)
                }

                override fun onResponse(call: Call<String>, response: Response<String>) {
                    response.let {
                        if(it.isSuccessful){
                            it.body()?.let { response ->
                                val obj = JSONObject(response)
                                val data = obj.getString("result")
                                val type = object : TypeToken<User?>() {}.type
                                val usuario = Gson().fromJson<User>(data, type)

                                completation(usuario, data, null)
                            }
                        }else{
                            completation(null, null, it.message())
                        }
                    }
                }
            })
        }

        fun addImageObject(idObject: String, imgb64: String, name: String, tipoImagen: String,
                           completation: (onFinish: Object?, onString: String?, onError: String?)-> Unit) {

            val body = HashMap<String, String>()
            body["id"] = idObject
            body["imgb64"] = imgb64
            body["name"] = name
            body["type"] = tipoImagen

            val call = WSOpemo.addImageObject(body)
            call.enqueue(object : Callback<String>{
                override fun onFailure(call: Call<String>, t: Throwable) {
                    completation(null, null, t.localizedMessage)
                }

                override fun onResponse(call: Call<String>, response: Response<String>) {
                    response.let {
                        if(it.isSuccessful){
                            it.body()?.let { response ->
                                val obj = JSONObject(response)
                                val data = obj.getString("result")
                                val type = object : TypeToken<Object?>() {}.type
                                val objectResponse = Gson().fromJson<Object>(data, type)

                                completation(objectResponse, data, null)
                            }
                        }else{
                            completation(null, null, it.message())
                        }
                    }
                }
            })
        }

        fun getListImageObject(idObject: String,
                               completation: (onFinish: List<ImageObject?>?, onString: String?, onError: String?)-> Unit) {

            val body = HashMap<String, String>()
            body["id"] = idObject

            val call = WSOpemo.getListImageObject(body)
            call.enqueue(object : Callback<String>{
                override fun onFailure(call: Call<String>, t: Throwable) {
                    completation(null, null, t.localizedMessage)
                }

                override fun onResponse(call: Call<String>, response: Response<String>) {
                    response.let {
                        if(it.isSuccessful){
                            it.body()?.let { response ->
                                val obj = JSONObject(response)
                                val data = obj.getString("result")
                                val type = object : TypeToken<List<ImageObject?>>() {}.type
                                val objectResponse = Gson().fromJson<List<ImageObject>>(data, type)

                                completation(objectResponse, data, null)
                            }
                        }else{
                            completation(null, null, it.message())
                        }
                    }
                }
            })
        }

        fun createObject(objeto: Object,
                         completation: (onFinish: Object?, onString: String?, onError: String?)-> Unit) {

            val body = HashMap<String, String>()
            body["idUser"] = objeto.idUser!!
            body["title"] = objeto.title!!
            body["description"] = objeto.description!!
            body["date"] = objeto.date!!
            body["latitude"] = objeto.latitude!!
            body["longitude"] = objeto.longitude!!

            val call = WSOpemo.createObject(body)
            call.enqueue(object : Callback<String>{
                override fun onFailure(call: Call<String>, t: Throwable) {
                    completation(null, null, t.localizedMessage)
                }

                override fun onResponse(call: Call<String>, response: Response<String>) {
                    response.let {
                        if(it.isSuccessful){
                            it.body()?.let { response ->
                                val obj = JSONObject(response)
                                val data = obj.getString("result")
                                val type = object : TypeToken<Object?>() {}.type
                                val objectResponse = Gson().fromJson<Object>(data, type)

                                completation(objectResponse, data, null)
                            }
                        }else{
                            completation(null, null, it.message())
                        }
                    }
                }
            })
        }

        fun deleteObject(id: String,
                         completation: (onFinish: String?, onError: String?)-> Unit) {

            val body = HashMap<String, String>()
            body["id"] = id

            val call = WSOpemo.deleteObject(body)
            call.enqueue(object : Callback<String>{
                override fun onFailure(call: Call<String>, t: Throwable) {
                    completation(null, t.localizedMessage)
                }

                override fun onResponse(call: Call<String>, response: Response<String>) {
                    response.let {
                        if(it.isSuccessful){
                            it.body()?.let { response ->
                                val obj = JSONObject(response)
                                val data = obj.getString("result")
                                completation(data, null)
                            }
                        }else{
                            completation(null, it.message())
                        }
                    }
                }
            })
        }

        fun readObjectAll(latitude: String, longitude: String,
                          completation: (onFinish: List<Object?>?, onString: String?, onError: String?)-> Unit) {

            val body = HashMap<String, String>()
            body["latitude"] = latitude
            body["longitude"] = longitude

            val call = WSOpemo.readObjectAll(body)
            call.enqueue(object : Callback<String>{
                override fun onFailure(call: Call<String>, t: Throwable) {
                    completation(null, null, t.localizedMessage)
                }

                override fun onResponse(call: Call<String>, response: Response<String>) {
                    response.let {
                        if(it.isSuccessful){
                            it.body()?.let { response ->
                                val obj = JSONObject(response)
                                val data = obj.getString("result")
                                val type = object : TypeToken<List<Object?>>() {}.type
                                val objectResponse = Gson().fromJson<List<Object>>(data, type)

                                completation(objectResponse, data, null)
                            }
                        }else{
                            completation(null, null, it.message())
                        }
                    }
                }
            })
        }

        fun readObjectUser(idUser: String,
                           completation: (onFinish: List<Object?>?, onString: String?, onError: String?)-> Unit) {

            val body = HashMap<String, String>()
            body["idUser"] = idUser

            val call = WSOpemo.readObjectUser(body)
            call.enqueue(object : Callback<String>{
                override fun onFailure(call: Call<String>, t: Throwable) {
                    completation(null, null, t.localizedMessage)
                }

                override fun onResponse(call: Call<String>, response: Response<String>) {
                    response.let {
                        if(it.isSuccessful){
                            it.body()?.let { response ->
                                val obj = JSONObject(response)
                                val data = obj.getString("result")
                                val type = object : TypeToken<List<Object?>>() {}.type
                                val objectResponse = Gson().fromJson<List<Object>>(data, type)

                                if(objectResponse.isEmpty()){
                                    completation(null, null, "nada")
                                }else {
                                    completation(objectResponse, data, null)
                                }
                            }
                        }else{
                            completation(null, null, it.message())
                        }
                    }
                }
            })
        }

        fun updateObject(objeto: Object,
                         completation: (onFinish: String?, onError: String?)-> Unit) {

            val body = HashMap<String, String>()
            body["id"] = objeto.id!!
            body["title"] = objeto.title!!
            body["description"] = objeto.description!!
            body["date"] = objeto.date!!
            body["status"] = objeto.status!!
            body["latitude"] = objeto.latitude!!
            body["longitude"] = objeto.longitude!!

            val call = WSOpemo.updateObject(body)
            call.enqueue(object : Callback<String>{
                override fun onFailure(call: Call<String>, t: Throwable) {
                    completation( null, t.localizedMessage)
                }

                override fun onResponse(call: Call<String>, response: Response<String>) {
                    response.let {
                        if(it.isSuccessful){
                            it.body()?.let { response ->
                                val obj = JSONObject(response)
                                val data = obj.getString("result")

                                completation(data, null)
                            }
                        }else{
                            completation(null, it.message())
                        }
                    }
                }
            })
        }


        fun createLike(idUsuario: String, idObjeto: String, fecha: String,
                       completation: (onFinish: String?, onError: String?)-> Unit) {

            val body = HashMap<String, String>()
            body["idUser"] = idUsuario
            body["idObject"] = idObjeto
            body["date"] = fecha

            val call = WSOpemo.createLike(body)
            call.enqueue(object : Callback<String>{
                override fun onFailure(call: Call<String>, t: Throwable) {
                    completation( null, t.localizedMessage)
                }

                override fun onResponse(call: Call<String>, response: Response<String>) {
                    response.let {
                        if(it.isSuccessful){
                            it.body()?.let { response ->
                                val obj = JSONObject(response)
                                val data = obj.getString("result")

                                completation(data, null)
                            }
                        }else{
                            completation(null, it.message())
                        }
                    }
                }
            })
        }

        fun readLike(idUsuario: String, idObjeto: String,
                     completation: (onFinish: String?, onError: String?)-> Unit) {

            val body = HashMap<String, String>()
            body["idUser"] = idUsuario
            body["idObject"] = idObjeto

            val call = WSOpemo.readLike(body)
            call.enqueue(object : Callback<String>{
                override fun onFailure(call: Call<String>, t: Throwable) {
                    completation( null, t.localizedMessage)
                }

                override fun onResponse(call: Call<String>, response: Response<String>) {
                    response.let {
                        if(it.isSuccessful){
                            it.body()?.let { response ->
                                val obj = JSONObject(response)
                                val data = obj.getString("result")

                                completation(data, null)
                            }
                        }else{
                            completation(null, it.message())
                        }
                    }
                }
            })
        }

        fun deleteLike(idUsuario: String, idObjeto: String,
                       completation: (onFinish: String?, onError: String?)-> Unit) {

            val body = HashMap<String, String>()
            body["IdUsuario"] = idUsuario
            body["idObjeto"] = idObjeto

            val call = WSOpemo.deleteLike(body)
            call.enqueue(object : Callback<String>{
                override fun onFailure(call: Call<String>, t: Throwable) {
                    completation( null, t.localizedMessage)
                }

                override fun onResponse(call: Call<String>, response: Response<String>) {
                    response.let {
                        if(it.isSuccessful){
                            it.body()?.let { response ->
                                val obj = JSONObject(response)
                                val data = obj.getString("result")

                                completation(data, null)
                            }
                        }else{
                            completation(null, it.message())
                        }
                    }
                }
            })
        }

        fun createMessage(idUsuario: String, idObjeto: String, mensaje: String, fecha: String,
                          completation: (onFinish: Message?, onError: String?)-> Unit) {

            val body = HashMap<String, String>()
            body["idUser"] = idUsuario
            body["idObject"] = idObjeto
            body["message"] = mensaje
            body["date"] = fecha

            val call = WSOpemo.createMessage(body)
            call.enqueue(object : Callback<String>{
                override fun onFailure(call: Call<String>, t: Throwable) {
                    completation( null, t.localizedMessage)
                }

                override fun onResponse(call: Call<String>, response: Response<String>) {
                    response.let {
                        if(it.isSuccessful){
                            it.body()?.let { response ->
                                val obj = JSONObject(response)
                                val data = obj.getString("result")
                                val type = object : TypeToken<Message>() {}.type
                                val objectResponse = Gson().fromJson<Message>(data, type)

                                completation(objectResponse, null)
                            }
                        }else{
                            completation(null, it.message())
                        }
                    }
                }
            })
        }

        fun readMessage(idObjeto: String,
                        completation: (onFinish: List<Message?>?, onString: String?, onError: String?)-> Unit) {

            val body = HashMap<String, String>()
            body["id"] = idObjeto

            val call = WSOpemo.readMessage(body)
            call.enqueue(object : Callback<String>{
                override fun onFailure(call: Call<String>, t: Throwable) {
                    completation( null, null, t.localizedMessage)
                }

                override fun onResponse(call: Call<String>, response: Response<String>) {
                    response.let {
                        if(it.isSuccessful){
                            it.body()?.let { response ->
                                val obj = JSONObject(response)
                                val data = obj.getString("result")
                                val type = object : TypeToken<List<Message?>?>() {}.type
                                val objectResponse = Gson().fromJson<List<Message?>?>(data, type)

                                completation(objectResponse, data, null)
                            }
                        }else{
                            completation(null, null, it.message())
                        }
                    }
                }
            })
        }

        fun updateMessage(id: String, idUsuario: String, idObjeto: String, mensaje: String, fecha: String,
                          completation: (onFinish: String?, onError: String?)-> Unit) {

            val body = HashMap<String, String>()
            body["id"] = id
            body["idUser"] = idUsuario
            body["idObject"] = idObjeto
            body["message"] = mensaje
            body["date"] = fecha

            val call = WSOpemo.updateMessage(body)
            call.enqueue(object : Callback<String>{
                override fun onFailure(call: Call<String>, t: Throwable) {
                    completation( null, t.localizedMessage)
                }

                override fun onResponse(call: Call<String>, response: Response<String>) {
                    response.let {
                        if(it.isSuccessful){
                            it.body()?.let { response ->
                                val obj = JSONObject(response)
                                val data = obj.getString("result")
                                completation(data, null)
                            }
                        }else{
                            completation(null, it.message())
                        }
                    }
                }
            })
        }

        fun deleteMessage(id: String,
                          completation: (onFinish: String?, onError: String?)-> Unit) {

            val body = HashMap<String, String>()
            body["id"] = id

            val call = WSOpemo.deleteMessage(body)
            call.enqueue(object : Callback<String>{
                override fun onFailure(call: Call<String>, t: Throwable) {
                    completation( null, t.localizedMessage)
                }

                override fun onResponse(call: Call<String>, response: Response<String>) {
                    response.let {
                        if(it.isSuccessful){
                            it.body()?.let { response ->
                                val obj = JSONObject(response)
                                val data = obj.getString("result")
                                completation(data, null)
                            }
                        }else{
                            completation(null, it.message())
                        }
                    }
                }
            })
        }










    }
}
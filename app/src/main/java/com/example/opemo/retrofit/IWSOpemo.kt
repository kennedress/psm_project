package com.example.opemo.retrofit

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import java.util.*

interface IWSOpemo {

    @POST("/Service1.svc/cUser")
    @Headers("Content-Type:application/json")
    fun createUser(@Body hashMap: HashMap<String, String>): Call<String>

    @POST("/Service1.svc/dUser")
    @Headers("Content-Type:application/json")
    fun deleteUser(@Body hashMap: HashMap<String, String>): Call<String>

    @POST("/Service1.svc/rLogin")
    @Headers("Content-Type:application/json")
    fun readLogin(@Body hashMap: HashMap<String, String>): Call<String>

    @POST("/Service1.svc/rUser")
    @Headers("Content-Type:application/json")
    fun readUser(@Body hashMap: HashMap<String, String>): Call<String>

    @POST("/Service1.svc/uUser")
    @Headers("Content-Type:application/json")
    fun updateUser(@Body hashMap: HashMap<String, String>): Call<String>

    @POST("/Service1.svc/profileImage")
    @Headers("Content-Type:application/json")
    fun updateImageProfile(@Body hashMap: HashMap<String, String>): Call<String>

    @POST("/Service1.svc/ImageObject")
    @Headers("Content-Type:application/json")
    fun addImageObject(@Body hashMap: HashMap<String, String>): Call<String>

    @POST("/Service1.svc/listimageobject")
    @Headers("Content-Type:application/json")
    fun getListImageObject(@Body hashMap: HashMap<String, String>): Call<String>

    @POST("/Service1.svc/cobject")
    @Headers("Content-Type:application/json")
    fun createObject(@Body hashMap: HashMap<String, String>): Call<String>

    @POST("/Service1.svc/dobject")
    @Headers("Content-Type:application/json")
    fun deleteObject(@Body hashMap: HashMap<String, String>): Call<String>

    @POST("/Service1.svc/robjectall")
    @Headers("Content-Type:application/json")
    fun readObjectAll(@Body hashMap: HashMap<String, String>): Call<String>

    @POST("/Service1.svc/robjectuser")
    @Headers("Content-Type:application/json")
    fun readObjectUser(@Body hashMap: HashMap<String, String>): Call<String>

    @POST("/Service1.svc/uobject")
    @Headers("Content-Type:application/json")
    fun updateObject(@Body hashMap: HashMap<String, String>): Call<String>


    @POST("/Service1.svc/clike")
    @Headers("Content-Type:application/json")
    fun createLike(@Body hashMap: HashMap<String, String>): Call<String>

    @POST("/Service1.svc/rlike")
    @Headers("Content-Type:application/json")
    fun readLike(@Body hashMap: HashMap<String, String>): Call<String>

    @POST("/Service1.svc/dlike")
    @Headers("Content-Type:application/json")
    fun deleteLike(@Body hashMap: HashMap<String, String>): Call<String>


    @POST("/Service1.svc/cmessage")
    @Headers("Content-Type:application/json")
    fun createMessage(@Body hashMap: HashMap<String, String>): Call<String>

    @POST("/Service1.svc/rmessage")
    @Headers("Content-Type:application/json")
    fun readMessage(@Body hashMap: HashMap<String, String>): Call<String>

    @POST("/Service1.svc/umessage")
    @Headers("Content-Type:application/json")
    fun updateMessage(@Body hashMap: HashMap<String, String>): Call<String>

    @POST("/Service1.svc/dmessage")
    @Headers("Content-Type:application/json")
    fun deleteMessage(@Body hashMap: HashMap<String, String>): Call<String>





    companion object {
        fun create(): IWSOpemo {
            val retrofit = Retrofit.Builder()
                .baseUrl("http://opemo.somee.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create(IWSOpemo::class.java)
        }
    }
}
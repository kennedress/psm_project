package com.example.opemo.service

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.view.View
import com.example.opemo.R
import com.example.opemo.adapter.RVImageObjectAdapter
import com.example.opemo.model.LocalImageObject
import com.example.opemo.model.Object
import java.net.URL

class LoadImageObject(
    private val listObject: MutableList<LocalImageObject>,
    private val name: String,
    private val mAdapter: RVImageObjectAdapter?)
    : AsyncTask<String, Void, Bitmap>() {

    override fun doInBackground(vararg params: String?): Bitmap? {
        return try{
            downloadBitmap(params[0])
        }catch (e: Exception){
            null
        }
    }

    override fun onPostExecute(result: Bitmap?) {
        super.onPostExecute(result)
        listObject.add(LocalImageObject(null, name, "", result, ".jpg"))
        mAdapter!!.notifyDataSetChanged()
    }

    private fun downloadBitmap(url: String?):Bitmap?{
        return try{
            val uri = URL(url)
            val conn = uri.openConnection()

            val inputStream = conn.getInputStream()
            if(inputStream != null){
                val bitmap = BitmapFactory.decodeStream(inputStream)
                bitmap
            }else{
                null
            }

        }catch (e: Exception){
            null
        }
    }
}
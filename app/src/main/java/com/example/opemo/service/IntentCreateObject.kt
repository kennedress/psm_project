package com.example.opemo.service

import android.app.IntentService
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Intent
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import com.example.opemo.R
import com.example.opemo.model.LocalImageObject
import com.example.opemo.model.Object
import com.example.opemo.retrofit.WSOpemo
import com.example.opemo.sqlite.DbImageObjectLocal
import com.example.opemo.sqlite.DbObjectLocal
import java.lang.Exception

class IntentCreateObject : IntentService("IntentCreateObject") {
    var uploadProgress = 0
    val notificationId = 666
    lateinit var nManager: NotificationManager
    lateinit var builder: Notification.Builder
    lateinit var builderCompat: NotificationCompat.Builder

    override fun onHandleIntent(intent: Intent?) {
        createNotification()
    }

    private fun createNotification() {
        nManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val mNotificationChannel = NotificationChannel(
                applicationContext.toString(),
                "service",
                //NotificationManager.IMPORTANCE_HIGH
                NotificationManager.IMPORTANCE_LOW
            )
            mNotificationChannel.enableLights(false)
            mNotificationChannel.lightColor = Color.BLUE
            mNotificationChannel.enableVibration(false)
            nManager.createNotificationChannel(mNotificationChannel)

            builder = Notification.Builder(this, applicationContext.toString())
                    .setOngoing(true)
                    .setSmallIcon(R.drawable.ic_file_upload_white_24dp)
                    .setContentTitle("Subiendo Información")
                    .setProgress(100, uploadProgress, false)


            nManager.notify(notificationId, builder.build())
            sendObject()

            /*for (i in 0..100 step 5) {
                builder.setProgress(100, i, false)
                nManager.notify(notificationId, builder.build())
                try {
                    Thread.sleep(1000)
                } catch (e: Exception) {
                    e.printStackTrace()
                    Log.d("Error", "sleep failure")
                }
            }

            builder.setContentTitle("Subida completada :3")
            nManager.notify(notificationId, builder.build())

            Thread.sleep(500)
            nManager.deleteNotificationChannel(applicationContext.toString())
            */
        } else {
            builderCompat = NotificationCompat.Builder(this, applicationContext.toString())
                .setOngoing(true)
                .setSmallIcon(R.drawable.ic_file_upload_white_24dp)
                .setContentTitle("Subiendo Información")
                .setProgress(100, 0, false)

            Thread(Runnable {
                for (i in 0..100 step 5) {
                    builderCompat.setProgress(100, i, false)
                    nManager.notify(notificationId, builderCompat.build())
                    try {
                        Thread.sleep(1000)
                    } catch (e: Exception) {
                        e.printStackTrace()
                        Log.d("Error", "sleep failure")
                    }
                }

                builderCompat.setContentTitle("Subida completada :3")
                nManager.notify(notificationId, builderCompat.build())

                Thread.sleep(500)
                nManager.cancel(notificationId)

            }).start()
        }
    }

    private fun sendObject(){
        val dbObject = DbObjectLocal(this)
        val dbObjectImage = DbImageObjectLocal(this)

        val cursorObject = dbObject.getAllRegisters()
        val cursorImage = dbObjectImage.getAllRegisters()

        if(cursorObject != null){
            if(cursorObject.count > 0){
                cursorObject.moveToFirst()

                val idLocalObject = cursorObject.getString(cursorObject.getColumnIndex(DbObjectLocal.COLUMN_ID))
                var objeto = Object(
                    null,
                    cursorObject.getString(cursorObject.getColumnIndex(DbObjectLocal.COLUMN_ID_USER)),
                    cursorObject.getString(cursorObject.getColumnIndex(DbObjectLocal.COLUMN_TITLE)),
                    cursorObject.getString(cursorObject.getColumnIndex(DbObjectLocal.COLUMN_DESCRIPTION)),
                    cursorObject.getString(cursorObject.getColumnIndex(DbObjectLocal.COLUMN_DATE)),
                    null,
                    cursorObject.getString(cursorObject.getColumnIndex(DbObjectLocal.COLUMN_LATITUDE)),
                    cursorObject.getString(cursorObject.getColumnIndex(DbObjectLocal.COLUMN_LONGITUDE)),
                    null)

                WSOpemo.createObject(objeto){ onFinish, _, onError ->
                    if(onError != null){ Toast.makeText(this, onError, Toast.LENGTH_LONG).show() }

                    if(onFinish != null){
                        Toast.makeText(this, onFinish.message, Toast.LENGTH_LONG).show()
                        if(onFinish.id != null){
                            objeto = onFinish
                            dbObject.itemDelete(idLocalObject)
                            uploadProgress += 20
                            builder.setProgress(100, uploadProgress, false)
                            nManager.notify(notificationId, builder.build())

                            if(cursorImage != null){
                                if(cursorImage.count > 0){
                                    val listImage: MutableList<LocalImageObject> = ArrayList()
                                    cursorImage.moveToFirst()
                                    do{
                                        val idImageObject = cursorImage.getString(cursorImage.getColumnIndex(DbImageObjectLocal.COLUMN_ID))
                                        listImage.add(LocalImageObject(
                                            null,
                                            cursorImage.getString(cursorImage.getColumnIndex(DbImageObjectLocal.COLUMN_IMAGE_NAME)),
                                            cursorImage.getString(cursorImage.getColumnIndex(DbImageObjectLocal.COLUMN_B64)),
                                            null,
                                            cursorImage.getString(cursorImage.getColumnIndex(DbImageObjectLocal.COLUMN_IMAGE_TYPE))
                                        ))
                                        dbObjectImage.itemDelete(idImageObject)
                                    }while(cursorImage.moveToNext())


                                    listImage.forEach {
                                        var porcentaje = 80 / listImage.size
                                        val total = porcentaje * listImage.size
                                        if(total < 80){
                                            porcentaje += (80 - total)
                                        }
                                        sendImage(objeto.id!!, it, porcentaje)
                                    }
                                }
                            }
                        }else{
                            Toast.makeText(this, onFinish.message, Toast.LENGTH_LONG).show()
                        }
                    }
                }
            }
        }
    }

    private fun sendImage(idObject: String, imageObject: LocalImageObject, incrementPB: Int){
        WSOpemo.addImageObject(idObject, imageObject.b64, imageObject.name, imageObject.type){ onFinish, _, onError ->
            if(onError != null){
                Toast.makeText(this, onError, Toast.LENGTH_LONG).show()
            }

            if(onFinish != null){
                if(onFinish.message == "Se Actualizó la imagen con éxito") {
                    uploadProgress += incrementPB

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        builder.setProgress(100, uploadProgress, false)
                        nManager.notify(notificationId, builder.build())
                    }else {
                        builderCompat.setProgress(100, uploadProgress, false)
                        nManager.notify(notificationId, builderCompat.build())
                    }

                    if(uploadProgress >= 100){
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            builder.setContentTitle("Subida completada :3")
                            nManager.notify(notificationId, builder.build())
                            Thread.sleep(1500)
                            nManager.deleteNotificationChannel(applicationContext.toString())
                        }else{
                            builderCompat.setContentTitle("Subida completada :3")
                            nManager.notify(notificationId, builderCompat.build())
                            Thread.sleep(500)
                            nManager.cancel(notificationId)
                        }
                    }
                }else{
                    Toast.makeText(this, onFinish.message, Toast.LENGTH_LONG).show()
                }
            }

        }
    }

    companion object {
    }
}

package com.example.opemo.service

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import com.example.opemo.R
import java.lang.ref.WeakReference
import java.net.URL

class LoadImage(private val imageViewReference : WeakReference<ImageView>,
                private val context: Context,
                private val pbWait: ProgressBar) : AsyncTask<String, Void, Bitmap>() {

    override fun doInBackground(vararg params: String?): Bitmap? {
        return try{
            downloadBitmap(params[0])
        }catch (e: Exception){
            null
        }
    }

    override fun onPostExecute(result: Bitmap?) {
        super.onPostExecute(result)
        val image = imageViewReference.get()
        if(image != null){
            if(result != null) {
                image.setImageBitmap(result)
                pbWait.visibility = View.GONE
            }else{
                image.setImageResource(R.drawable.ic_person_orange_24dp)
                pbWait.visibility = View.GONE
            }
        }
    }

    private fun downloadBitmap(url: String?):Bitmap?{
        return try{
            val uri = URL(url)
            val conn = uri.openConnection()

            val inputStream = conn.getInputStream()
            if(inputStream != null){
                val bitmap = BitmapFactory.decodeStream(inputStream)
                bitmap
            }else{
                null
            }

        }catch (e: Exception){
            null
        }
    }
}
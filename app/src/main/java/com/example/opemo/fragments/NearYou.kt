package com.example.opemo.fragments

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.opemo.Constants
import com.example.opemo.R
import com.example.opemo.activities.MainActivity
import com.example.opemo.activities.ReadUpdateObject
import com.example.opemo.activities.RegisterObject
import com.example.opemo.model.Object
import com.example.opemo.retrofit.WSOpemo
import com.google.android.gms.location.LocationListener
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_near_you.*

class NearYou : Fragment(), OnMapReadyCallback,
    GoogleMap.OnMarkerClickListener,
    GoogleMap.OnCameraMoveListener,
    GoogleMap.OnInfoWindowClickListener,
    LocationListener {

    private lateinit var mMapView: MapView
    private lateinit var mMap: GoogleMap
    private var myLocation: Location? = null
    private var listener: OnFragmentInteractionListener? = null
    private var mContext: Context? = null
    private var listObject: List<Object?>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val inflate = inflater.inflate(R.layout.fragment_near_you, container, false)
        var mapViewBundle: Bundle? = null
        if(savedInstanceState != null){
            mapViewBundle = savedInstanceState.getBundle(Constants.MAPVIEW_BUNDLE_KEY)
        }

        mMapView = inflate.findViewById(R.id.mv_map) as MapView
        mMapView.onCreate(mapViewBundle)
        mMapView.getMapAsync(this)
        MapsInitializer.initialize(mContext)

        val fabCreateObject  = inflate.findViewById<FloatingActionButton>(R.id.fab_create_object)
        fabCreateObject.setOnClickListener {
            activity!!.startActivityForResult(RegisterObject.newIntent(mContext!!,
                (mContext as MainActivity).stringUser,
                myLocation!!.latitude,
                myLocation!!.longitude), Constants.C_OBJECT_REQUEST)
        }

        return inflate
    }

    override fun onResume() {
        super.onResume()
        mMapView.onResume()
    }

    override fun onStart() {
        super.onStart()
        mMapView.onStart()
    }

    override fun onStop() {
        super.onStop()
        mMapView.onStop()
    }

    override fun onPause() {
        mMapView.onPause()
        super.onPause()
    }

    override fun onDestroy() {
        mMapView.onDestroy()
        super.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mMapView.onLowMemory()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        mMap.uiSettings.isMapToolbarEnabled = false
        mMap.uiSettings.isZoomControlsEnabled = true
        mMap.setOnMarkerClickListener(this)
        mMap.setOnCameraMoveListener(this)
        mMap.setOnInfoWindowClickListener(this)

        if(ActivityCompat.checkSelfPermission(mContext!!, Manifest.permission.ACCESS_FINE_LOCATION) !=
            PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(mContext!!, Manifest.permission.ACCESS_COARSE_LOCATION) !=
            PackageManager.PERMISSION_GRANTED){
            return
        }
        mMap.isMyLocationEnabled = true
        val locationManager = mContext!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        myLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)

        val lat = myLocation!!.latitude
        val lng = myLocation!!.longitude

        val camera = CameraUpdateFactory.newLatLngZoom(LatLng(lat, lng), 20f)
        mMap.moveCamera(camera)

        fetchObjects()
    }

    override fun onMarkerClick(p0: Marker?): Boolean {
       return false
    }

    override fun onInfoWindowClick(p0: Marker?) {
        listObject?.forEach {
            if(it?.id == p0?.snippet){
                val stringObject = Gson().toJson(it)
                val intent = ReadUpdateObject.newIntent(mContext!!, stringObject, (mContext as MainActivity).stringUser)
                activity!!.startActivityForResult(intent, Constants.C_OBJECT_REQUEST)
            }
        }
    }

    override fun onCameraMove() {

    }

    override fun onLocationChanged(p0: Location?) {

    }

    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
            mContext = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener { fun onFragmentInteraction(uri: Uri) }


    fun fetchObjects(){
        WSOpemo.readObjectAll(myLocation!!.latitude.toString(), myLocation!!.longitude.toString()){
            onFinish, onString, onError ->

            if(onError != null){ Toast.makeText(mContext, onError, Toast.LENGTH_LONG).show() }

            if(onString != null){
                listObject = onFinish
                onFinish?.forEach {
                    if(it != null){
                        // Aquí dibujamos todos los objetos
                        val markerOptions = MarkerOptions()
                            .title(it.title)
                            .position(LatLng(it.latitude!!.toDouble(), it.longitude!!.toDouble()))
                            .snippet(it.id)

                        mMap.addMarker(markerOptions)
                    }
                }
            }
        }
    }

    fun cleanMarker(){ mMap.clear() }

    companion object {

    }
}

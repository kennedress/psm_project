package com.example.opemo.fragments

import android.app.AlertDialog
import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.opemo.R
import com.example.opemo.model.User

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [PerfilFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [PerfilFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PerfilFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    var requestCodeTemp = 0
    val requestCodeUpdate = 100
    lateinit var usuario: User
    private var listener: OnFragmentInteractionListener? = null
    var mContext: Context? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val inflate = inflater.inflate(R.layout.fragment_chat, container, false)
        /*usuario = (mContext as MainActivity).user!!

        inflate.btn_close_session.setOnClickListener { Constants.closeSession(context) }
        inflate.btn_change.setOnClickListener { updateUser() }

        inflate.textView4.setText(usuario.nick)
        inflate.textView5.setText(usuario.email)*/
        return inflate
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    private fun updateUser(){
        /*val intent = Intent(mContext, update_user::class.java)
        intent.putExtra(Utils.PUT_EXTRA_USER, Gson().toJson(usuario))

        startActivityForResult(intent, requestCodeUpdate)*/


    }

    private fun takePhoto(requestCode: Int) {
        val builder = AlertDialog.Builder(mContext)
        builder.setTitle("Tomar Fotografía")
        builder.setPositiveButton("Tomar fotografía") { _, _ ->
            requestCodeTemp = requestCode
            //EasyImage.openCamera(this, 0)
        }
//        builder.setNeutralButton("Elegir foto") { _, _ ->
//            requestCodeTemp = requestCode
//            EasyImage.openGallery(this, 0)
//        }
        builder.setNegativeButton("Cancelar") { dialog, _ ->
            dialog.dismiss()
        }

        builder.show()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
            mContext = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment PerfilFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            PerfilFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}

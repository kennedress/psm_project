package com.example.opemo.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.opemo.fragments.NearYou
import com.example.opemo.fragments.PerfilFragment
import com.example.opemo.fragments.PostFragment

class VPAdapter(private val myContext: Context, fm: FragmentManager, internal var totalTabs: Int) : FragmentPagerAdapter(fm) {

    val nearYou = NearYou()
    val postFragment = PostFragment()
    val perfilFragment = PerfilFragment()

    // this is for fragment tabs
    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return nearYou
            1 -> return postFragment
            2 -> return perfilFragment
            else -> return nearYou
        }
    }

    // this counts total number of tabs
    override fun getCount(): Int {
        return totalTabs
    }
}
package com.example.opemo.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.opemo.R
import com.example.opemo.activities.ReadUpdateObject
import com.example.opemo.model.Object
import com.google.gson.Gson

class RVMyPublicationsAdapter(
    private val context: Context,
    private val listObject: List<Object?>,
    private val stringUser: String)
    : RecyclerView.Adapter<RVMyPublicationsAdapter.ObjectHolder>() {

    inner class ObjectHolder(v: View) : RecyclerView.ViewHolder(v){
        private val title = v.findViewById<TextView>(R.id.tv_object_title)
        private val fecha = v.findViewById<TextView>(R.id.tv_object_date)
        private val itemObject = v.findViewById<ConstraintLayout>(R.id.cl_object)

        fun bind(item: Object?, context: Context, position: Int){
            title.text = item!!.title
            fecha.text = item.date

            if(position % 2 == 0){
                itemObject.background = ContextCompat.getDrawable(context, R.color.silver)
            }

            itemObject.setOnClickListener {
                val stringObject = Gson().toJson(item)
                val intent = ReadUpdateObject.newIntent(context, stringObject, stringUser)
                context.startActivity(intent)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ObjectHolder {
        return ObjectHolder(LayoutInflater.from(context).inflate(R.layout.custom_list_object, parent, false))
    }

    override fun getItemCount() = listObject.size

    override fun onBindViewHolder(holder: ObjectHolder, position: Int) {
        holder.bind(listObject[position], context, position)
    }

}
package com.example.opemo.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.opemo.R
import com.example.opemo.model.LocalImageObject

class RVImageObjectAdapter(
    private val context: Context,
    private val photos: MutableList<LocalImageObject>,
    private val createOrRead: Boolean)
    : RecyclerView.Adapter<RVImageObjectAdapter.PhotoHolder>() {

    inner class PhotoHolder(v: View) : RecyclerView.ViewHolder(v){
        private val image = v.findViewById<ImageView>(R.id.iv_image)
        private val deleteImage = v.findViewById<ImageButton>(R.id.ib_delete_image)

        fun bind(photo: LocalImageObject, context: Context){
            image.setImageBitmap(photo.image)
            if(createOrRead){
                deleteImage.visibility = View.GONE
            }
            deleteImage.setOnClickListener {
                photos.removeAt(layoutPosition)
                notifyItemRemoved(layoutPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoHolder {
        return PhotoHolder(LayoutInflater.from(context).inflate(R.layout.custom_view_register_object_image, parent, false))
    }

    override fun getItemCount() = photos.size

    override fun onBindViewHolder(holder: PhotoHolder, position: Int) {
        holder.bind(photos[position], context)
    }
}
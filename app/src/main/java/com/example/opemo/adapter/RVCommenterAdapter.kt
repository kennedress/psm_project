package com.example.opemo.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.opemo.R
import com.example.opemo.model.Message

class RVCommenterAdapter(
    private val context: Context,
    private val comentarios: List<Message?>,
    private val idUser: String)
    : RecyclerView.Adapter<RVCommenterAdapter.CommenterHolder>() {

    inner class CommenterHolder(v: View) : RecyclerView.ViewHolder(v){
        private val tvUser = v.findViewById<TextView>(R.id.tv_how_commenter)
        private val tvDate = v.findViewById<TextView>(R.id.tv_date)
        private val tvCommenter = v.findViewById<TextView>(R.id.tv_commenter)
        private val ibEdit = v.findViewById<ImageButton>(R.id.ib_edit_commenter)
        private val ibDelete = v.findViewById<ImageButton>(R.id.ib_delete_commenter)

        fun bind(item: Message?){
            if(item != null){
                tvUser.text = item.user
                tvDate.text = item.date
                tvCommenter.text = item.mensajes

                if(item.idUser == idUser){
                    ibEdit.visibility = View.VISIBLE
                    ibDelete.visibility = View.VISIBLE

                    ibEdit.setOnClickListener {

                    }

                    ibDelete.setOnClickListener {

                    }
                }
            }
        }
    }

    override fun getItemCount() = comentarios.size


    override fun onBindViewHolder(holder: CommenterHolder, position: Int) {
        holder.bind(comentarios[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommenterHolder {
        return CommenterHolder(LayoutInflater.from(context).inflate(R.layout.custom_commenter, parent, false))
    }
}
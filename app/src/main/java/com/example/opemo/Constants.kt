package com.example.opemo

import android.Manifest
import android.content.Context

/**
 * Created by Kenneth Briones on 06/11/2019.
 */
class Constants {
    companion object{
        const val name_pref = "usuario"
        const val SENSOR_ACTIVATE = "SENSOR_ACTIVATE"

        const val pathObject = "http://www.opemo.somee.com/pics/objeto/"
        const val pathProfile = "http://www.opemo.somee.com/pics/profile/"
        const val typeFormatImage = ".jpeg"

        const val MAPVIEW_BUNDLE_KEY = "mapview_bundle_key"

        const val camera_manifest = Manifest.permission.CAMERA
        const val external_storage = Manifest.permission.WRITE_EXTERNAL_STORAGE

        const val CAMERA_PERMISSION = 100
        const val CAMERA_REQUEST = 101
        const val GALLERY_REQUEST = 102
        const val C_OBJECT_REQUEST = 103
        const val U_USER_REQUEST = 104

        fun closeSession(context: Context?){
            val sharedPreferences = context!!.getSharedPreferences(name_pref, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString("usu", "")
            editor.putBoolean(SENSOR_ACTIVATE, false)
            editor.apply()
        }

        
    }
}
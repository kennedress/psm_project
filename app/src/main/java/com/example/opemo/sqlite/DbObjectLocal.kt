package com.example.opemo.sqlite

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.example.opemo.model.Object

class DbObjectLocal(context: Context):
    SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION){

    override fun onCreate(db: SQLiteDatabase?) {
        val table = ("CREATE TABLE " +
                TABLE_NAME + "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY, " +
                COLUMN_ID_USER + " TEXT, " +
                COLUMN_TITLE + " TEXT, " +
                COLUMN_DESCRIPTION + " TEXT, " +
                COLUMN_DATE + " TEXT, " +
                COLUMN_LATITUDE + " TEXT, " +
                COLUMN_LONGITUDE + " TEXT" +
                ")")
        db!!.execSQL(table)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
    }

    fun addLocalImageObject(objeto: Object){
        val values = ContentValues()
        values.put(COLUMN_ID_USER, objeto.idUser)
        values.put(COLUMN_TITLE, objeto.title)
        values.put(COLUMN_DESCRIPTION, objeto.description)
        values.put(COLUMN_DATE, objeto.date)
        values.put(COLUMN_LATITUDE, objeto.latitude)
        values.put(COLUMN_LONGITUDE, objeto.longitude)

        val db = this.writableDatabase
        if(db.isOpen) {
            val result = db.insert(TABLE_NAME, null, values)
            Log.d("DbObjectLocal", result.toString())
            db.close()
        }
    }

    fun getAllRegisters(): Cursor?{
        val db = this.readableDatabase
        if(db.isOpen){
            val columns = arrayOf(COLUMN_ID, COLUMN_ID_USER, COLUMN_TITLE, COLUMN_DESCRIPTION, COLUMN_DATE, COLUMN_LATITUDE, COLUMN_LONGITUDE)

            val cursor = db.query(TABLE_NAME, columns, null, null, null, null, null, null)
            return if(cursor.count > 0)
                cursor
            else
                null
        }
        return null
    }

    fun itemDelete(id: String){
        val db = this.writableDatabase
        if(db.isOpen){
            db.delete(TABLE_NAME, id, null)
            db.close()
        }
    }

    fun deleteAllTable(){
        val db = this.writableDatabase
        if(db.isOpen) {
            db.delete(TABLE_NAME, null, null)
            db.close()
        }
    }

    companion object{
        private const val DATABASE_NAME = "object.db"
        private const val DATABASE_VERSION = 1

        val TABLE_NAME = "object"
        val COLUMN_ID = "id"
        val COLUMN_ID_USER = "id_user"
        val COLUMN_TITLE = "title"
        val COLUMN_DESCRIPTION = "description"
        val COLUMN_DATE = "date"
        val COLUMN_LATITUDE = "latitude"
        val COLUMN_LONGITUDE = "longitude"

    }
}
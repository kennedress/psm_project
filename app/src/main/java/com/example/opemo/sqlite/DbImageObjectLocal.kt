package com.example.opemo.sqlite

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.example.opemo.model.LocalImageObject

class DbImageObjectLocal(context: Context):
    SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION){

    override fun onCreate(db: SQLiteDatabase?) {
        val table = ("CREATE TABLE " +
                TABLE_NAME + "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY, " +
                COLUMN_ID_OBJECT + " TEXT, " +
                COLUMN_IMAGE_NAME + " TEXT, " +
                COLUMN_B64 + " TEXT, " +
                COLUMN_IMAGE_TYPE + " TEXT" +
                ")")
        db!!.execSQL(table)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
    }

    fun addLocalImageObject(image: LocalImageObject){
        val values = ContentValues()
        values.put(COLUMN_ID_OBJECT, image.id)
        values.put(COLUMN_IMAGE_NAME, image.name)
        values.put(COLUMN_B64, image.b64)
        values.put(COLUMN_IMAGE_TYPE, image.type)

        val db = this.writableDatabase
        if(db.isOpen) {
            val result = db.insert(TABLE_NAME, null, values)
            Log.d("DbImageObject", result.toString())
            db.close()
        }
    }

    fun getAllRegisters(): Cursor?{
        val db = this.readableDatabase
        if(db.isOpen){
            val columns = arrayOf(COLUMN_ID, COLUMN_ID_OBJECT,COLUMN_IMAGE_NAME,COLUMN_B64,COLUMN_IMAGE_TYPE)

            val cursor = db.query(TABLE_NAME, columns, null, null, null, null, null, null)
            return if(cursor.count > 0)
                cursor
            else
                null
        }
        return null
    }

    fun itemDelete(id: String){
        val db = this.writableDatabase
        if(db.isOpen){
            db.delete(TABLE_NAME, id, null)
            db.close()
        }
    }

    fun deleteAllTable(){
        val db = this.writableDatabase
        if(db.isOpen) {
            db.delete(TABLE_NAME, null, null)
            db.close()
        }
    }

    companion object{
        private const val DATABASE_NAME = "imageObject.db"
        private const val DATABASE_VERSION = 1

        val TABLE_NAME = "image_object"
        val COLUMN_ID = "id"
        val COLUMN_ID_OBJECT = "id_object"
        val COLUMN_IMAGE_NAME = "image_name"
        val COLUMN_B64 = "b64"
        val COLUMN_IMAGE_TYPE = "image_type"
    }
}
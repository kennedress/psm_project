package com.example.opemo.activities

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.widget.addTextChangedListener
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.example.opemo.Constants
import com.example.opemo.R
import com.example.opemo.retrofit.WSOpemo
import com.facebook.share.model.ShareLinkContent
import com.facebook.share.widget.ShareDialog
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_register_object.*


class RegisterUser : AppCompatActivity() {

    private var initSession = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        val toolbar: Toolbar = findViewById(R.id.tb_register_user)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        initController()
    }

    override fun onResume() {
        super.onResume()
        if(initSession){
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home ->{ onBackPressed() }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if(reviewEnableRegister()) {
            super.onBackPressed()
        }else{
            AlertDialog.Builder(this).setTitle(R.string.app_name as Int)
                .setMessage(R.string.register_user_return_message)
                .setCancelable(false)
                .setPositiveButton(R.string.yes) { _, _ -> super.onBackPressed() }
                .setNegativeButton(R.string.no) { dialog, _ -> dialog.dismiss() }
                .create()
                .show()
        }
    }

    private fun reviewEnableRegister(): Boolean{
        return register_et_nombre.text.isNullOrEmpty() &&
                register_et_email.text.isNullOrEmpty() &&
                register_et_contra.text.isNullOrEmpty() &&
                register_et_confirm_contra.text.isNullOrEmpty()
    }

    private fun facebookPublish(): String{
        val FACEBOOK_URL = "https://www.facebook.com/YourPageName"
        val FACEBOOK_PAGE_ID = "560311091284099"
        val packageManager = this.packageManager
        return try{
            val versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode
            if(versionCode >= 30028500){
                "fb://facewebmodal/f?href=$FACEBOOK_URL"
            }else{
                "fb://page/$FACEBOOK_PAGE_ID"
            }
        }catch (e: PackageManager.NameNotFoundException){
            FACEBOOK_URL
        }
    }

    private fun initController(){

        btn_register_user.setOnClickListener {
            val name = register_et_nombre!!.text.toString()
            val email = register_et_email!!.text.toString()
            val pass = register_et_contra!!.text.toString()
            val confirmPass = register_et_confirm_contra!!.text.toString()

            if((name != "" && email != "" && pass != "" && confirmPass != "") && pass == confirmPass) {

                AlertDialog.Builder(this).setTitle(R.string.app_name)
                    .setMessage(R.string.facebook_share)
                    .setCancelable(false)
                    .setPositiveButton(R.string.yes) { dialog, _ ->

                        val message = getString(R.string.facebook_message, name)
                        val content = ShareLinkContent.Builder()
                            .setQuote(message)
                            .setContentUrl(Uri.parse("http://extremejapanesechallenge.somee.com"))
                            .build()
                        val shareDialog = ShareDialog(this)
                        shareDialog.show(content)
                        register(name, pass, email, true)
                        dialog.dismiss()
                    }
                    .setNegativeButton(R.string.no) { dialog, _ ->
                        register(name, pass, email, false)
                        dialog.dismiss()
                    }
                    .create()
                    .show()
            }else{
                if(pass != confirmPass){
                    YoYo.with(Techniques.Tada)
                        .duration(700)
                        .repeat(1)
                        .playOn(editText4)

                    YoYo.with(Techniques.Tada)
                        .duration(700)
                        .repeat(1)
                        .playOn(editText7)
                }else{
                    if(pass == ""){
                        YoYo.with(Techniques.Tada)
                            .duration(700)
                            .repeat(1)
                            .playOn(editText4)
                    }

                    if(confirmPass == ""){
                        YoYo.with(Techniques.Tada)
                            .duration(700)
                            .repeat(1)
                            .playOn(editText7)
                    }
                }

                if(name == ""){
                    YoYo.with(Techniques.Tada)
                        .duration(700)
                        .repeat(1)
                        .playOn(editText3)

                }

                if(email == ""){
                    YoYo.with(Techniques.Tada)
                        .duration(700)
                        .repeat(1)
                        .playOn(editText5)

                }
            }
        }
    }

    private fun register(name: String, pass: String, email: String, det: Boolean){
        WSOpemo.getCUser(name, pass, email){ onFinish, onString, onError ->
            if(onError != null){
                Toast.makeText(this, onError, Toast.LENGTH_LONG).show()
            }

            if(onString != null && onFinish != null){

                if(onFinish.id != null){
                    val sharedPreferences  = getSharedPreferences(Constants.name_pref, Context.MODE_PRIVATE)
                    val editor = sharedPreferences.edit()
                    editor.putString("usu", onString)
                    editor.apply()
                    initSession = det
                    if(!det) {
                        startActivity(Intent(this, MainActivity::class.java))
                        finish()
                    }
                }else{
                    Toast.makeText(this, onFinish.message, Toast.LENGTH_LONG).show()
                }
            }
        }
    }
}

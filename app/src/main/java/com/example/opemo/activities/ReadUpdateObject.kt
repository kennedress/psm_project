package com.example.opemo.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.example.opemo.Constants
import com.example.opemo.R
import com.example.opemo.adapter.RVImageObjectAdapter
import com.example.opemo.model.LocalImageObject
import com.example.opemo.model.Message
import com.example.opemo.model.Object
import com.example.opemo.model.User
import com.example.opemo.retrofit.WSOpemo
import com.example.opemo.service.LoadImageObject
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_read_update_object.*
import kotlinx.android.synthetic.main.activity_read_update_object.rv_photo_object
import kotlinx.android.synthetic.main.activity_read_update_object.tiet_description
import kotlinx.android.synthetic.main.activity_read_update_object.tiet_title
import java.lang.StringBuilder
import java.text.SimpleDateFormat
import java.util.*

class ReadUpdateObject : AppCompatActivity(),
    SensorEventListener {

    private var objetoSeleccionado: Object? = null
    private var userActivo: User? = null
    val listImage: MutableList<LocalImageObject> = arrayListOf()
    var listComenters: MutableList<Message> = arrayListOf()
    var listcomentersString: String? = null
    var mAdapter: RVImageObjectAdapter? = null

    private lateinit var mSensorManager: SensorManager
    private var mSensors: Sensor? = null
    private var sensorActivate = false

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) { }

    override fun onSensorChanged(p0: SensorEvent?) {
        // Sensor change value
        val millibarsOfPressure = p0!!.values[0].toInt()
        if (p0.sensor.type == Sensor.TYPE_LIGHT){
            when(millibarsOfPressure){
                in 0..30 ->{ colorDark() }
                in 31..100-> { colorNormal() }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_read_update_object)

        val toolbar: Toolbar = findViewById(R.id.tb_read_update_object)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        initSensor()
        initSp()

        initController()
        returnInformation()

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home -> {
                setResult(Activity.RESULT_CANCELED)
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        initSp()
    }

    override fun onPause() {
        super.onPause()
        if(sensorActivate)
            mSensorManager.unregisterListener(this)
    }

    private fun initSp(){
        val sharedPreferences = getSharedPreferences(Constants.name_pref, Context.MODE_PRIVATE)
        sensorActivate = sharedPreferences.getBoolean(Constants.SENSOR_ACTIVATE, false)
        if(sensorActivate)
            mSensorManager.registerListener(this, mSensors, SensorManager.SENSOR_DELAY_NORMAL)
        else{
            mSensorManager.unregisterListener(this)
            colorNormal()
        }
    }

    private fun initSensor(){
        mSensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        mSensors = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT)
    }

    private fun colorNormal(){
        theme.applyStyle(R.style.AppTheme, true)
        window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimary)
        tb_read_update_object.background = getDrawable(R.color.background_layout)
        tiet_title.background = getDrawable(R.color.background_tiet)
    }

    private fun colorDark(){
        theme.applyStyle(R.style.AppThemeDark, true)
        window.statusBarColor = ContextCompat.getColor(this, R.color.background_layout_dark)

        tb_read_update_object.background = getDrawable(R.color.background_layout_dark)
        tiet_title.background = getDrawable(R.color.silver)
    }

    private fun returnInformation(){
        intent.getStringExtra(PUT_EXTRA_USER)?.let{
            val type = object : TypeToken<User>(){ }.type
            userActivo = Gson().fromJson(it, type)
        }

        intent.getStringExtra(PUT_EXTRA_OBJECT)?.let{
            val type = object : TypeToken<Object>(){ }.type
            objetoSeleccionado = Gson().fromJson(it, type)

            tiet_title.setText(objetoSeleccionado?.title)
            tv_title.setText(objetoSeleccionado?.title)
            tiet_description.setText(objetoSeleccionado?.description)
            tv_description.setText(objetoSeleccionado?.description)

            WSOpemo.getListImageObject(objetoSeleccionado?.id!!){ onFinish, onString, onError ->
                if(onError != null){ Toast.makeText(this, onError, Toast.LENGTH_LONG).show() }

                if(onString != null){
                    onFinish?.forEach { nombreImage ->
                        if(nombreImage?.message == "sin problema"){
                            val sb = StringBuilder()
                            sb.append(Constants.pathObject)
                            sb.append(nombreImage.name)
                            sb.append(Constants.typeFormatImage)
                            LoadImageObject(listImage, nombreImage.name!!, mAdapter).execute(sb.toString())
                        }
                    }
                }
            }

            btn_take_photo.visibility = View.GONE
            btn_clear_photo.visibility = View.GONE

            when(objetoSeleccionado?.status!!){
                "-1" -> { sp_status.setSelection(3) }
                "0" -> { sp_status.setSelection(2) }
                "1" -> { sp_status.setSelection(0) }
                "2" -> { sp_status.setSelection(1) }
            }


            if(userActivo?.id != objetoSeleccionado?.idUser){
                sp_status.isEnabled = false
                btn_save_object.visibility = View.INVISIBLE
                til_title.visibility = View.GONE
                til_description.visibility = View.INVISIBLE
            }else{
                tv_title.visibility = View.INVISIBLE
                tv_description.visibility = View.INVISIBLE
            }

            readLikeObject()
            readMessageObject()
        }
    }

    private fun initController(){
        rv_photo_object.setHasFixedSize(true)
        mAdapter = RVImageObjectAdapter(this, listImage, true)
        rv_photo_object.adapter = mAdapter

        btn_save_object.setOnClickListener {
            val index = sp_status.selectedItemId.toInt()
            if(index == 3){
                WSOpemo.deleteObject(objetoSeleccionado!!.id!!){ onFinish, onError ->
                    if(onError != null){ Toast.makeText(this, onError, Toast.LENGTH_LONG).show() }

                    if(onFinish != null){
                        Toast.makeText(this, onFinish, Toast.LENGTH_LONG).show()
                        setResult(Activity.RESULT_OK)
                        onBackPressed()
                    }
                }
            }else{
                objetoSeleccionado?.title = tiet_title.text.toString()
                objetoSeleccionado?.description = tiet_description.text.toString()
                when(index){
                    0 -> { objetoSeleccionado?.status = "1" }
                    1 -> { objetoSeleccionado?.status = "2" }
                    2 -> { objetoSeleccionado?.status = "0" }
                    else -> { objetoSeleccionado?.status = "1" }
                }

                WSOpemo.updateObject(objetoSeleccionado!!){ onFinish, onError ->
                    if(onError != null){ Toast.makeText(this, onError, Toast.LENGTH_LONG).show() }

                    if(onFinish != null){
                        Toast.makeText(this, onFinish, Toast.LENGTH_LONG).show()
                        setResult(Activity.RESULT_OK)
                        onBackPressed()
                    }
                }
            }
        }

        btn_like.setOnClickListener {
            if(btn_like.text == getString(R.string.btn_like)){
                val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
                WSOpemo.createLike(userActivo?.id!!, objetoSeleccionado?.id!!, sdf.format(Date()).toString()){
                    onFinish, onError ->

                    if(onError != null){ Toast.makeText(this, onError, Toast.LENGTH_LONG).show() }

                    if(onFinish != null){
                        if(onFinish == "Se agregó un like."){
                            readLikeObject()
                        }else{
                            Toast.makeText(this, onFinish, Toast.LENGTH_LONG).show()
                        }
                    }
                }
            }else{
                val idu =  userActivo?.id!!
                val ido = objetoSeleccionado?.id!!
                WSOpemo.deleteLike(userActivo?.id!!, objetoSeleccionado?.id!!){ onFinish, onError ->
                    if(onError != null){ Toast.makeText(this, onError, Toast.LENGTH_LONG).show() }

                    if(onFinish != null){
                        if(onFinish == "Se quito el like."){
                            readLikeObject()
                        }else{
                            Toast.makeText(this, onFinish, Toast.LENGTH_LONG).show()
                        }
                    }
                }
            }
        }

        btn_comenter.setOnClickListener {
            val intent = CRUDMessage.newIntent(this, userActivo?.id!!, objetoSeleccionado?.id!!, listcomentersString)
            startActivity(intent)
        }
    }

    private fun readLikeObject(){
        WSOpemo.readLike(userActivo?.id!!, objetoSeleccionado?.id!!){ onFinish, onError ->
            if(onError != null){ Toast.makeText(this, onError, Toast.LENGTH_LONG).show() }

            if(onFinish != null){
                val separator = onFinish.split("|")
                tv_likes.text = getString(R.string.tv_likes, separator[0])

                // Me gusta
                if(separator[1] == "0"){
                    btn_like.text = getString(R.string.btn_like)
                }else{ // No me gusta
                    btn_like.text = getString(R.string.btn_dont_like)
                }
            }
        }
    }

    private fun readMessageObject(){

    }

    companion object{
        const val PUT_EXTRA_OBJECT = "PUT_EXTRA_OBJECT"
        const val PUT_EXTRA_USER = "PUT_EXTRA_USER"

        fun newIntent(context: Context, objeto: String?, user: String?): Intent {
            val intent =  Intent(context, ReadUpdateObject::class.java)
            intent.putExtra(PUT_EXTRA_OBJECT, objeto)
            intent.putExtra(PUT_EXTRA_USER, user)
            return intent
        }
    }
}

package com.example.opemo.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.core.view.forEach
import androidx.fragment.app.FragmentPagerAdapter
import com.example.opemo.Constants
import com.example.opemo.R
import com.example.opemo.adapter.VPAdapter
import com.example.opemo.fragments.NearYou
import com.example.opemo.fragments.PerfilFragment
import com.example.opemo.fragments.PostFragment
import com.example.opemo.model.User
import com.example.opemo.service.LoadImage
import com.google.android.material.navigation.NavigationView
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_nav_settings.*
import kotlinx.android.synthetic.main.activity_nav_update_user.*
import java.lang.ref.WeakReference


class MainActivity : AppCompatActivity(),
    NearYou.OnFragmentInteractionListener,
    PerfilFragment.OnFragmentInteractionListener,
    PostFragment.OnFragmentInteractionListener,
    NavigationView.OnNavigationItemSelectedListener,
    SensorEventListener {

    private lateinit var mSensorManager: SensorManager
    private var mSensors: Sensor? = null
    private var sensorActivate = false

    var stringUser: String? = null
    lateinit var adapter: FragmentPagerAdapter

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) { }

    override fun onSensorChanged(p0: SensorEvent?) {
        // Sensor change value
        val millibarsOfPressure = p0!!.values[0].toInt()
        if (p0.sensor.type == Sensor.TYPE_LIGHT){
            when(millibarsOfPressure){
                in 0..30 ->{ colorDark() }
                in 31..100-> { colorNormal() }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initSensor()
        initSp()

        adapter = VPAdapter(this, supportFragmentManager, tl_home.tabCount)
        returnInformation()
        configurationBar()
        tlVpInit()
    }

    override fun onBackPressed() {
        if(drawer_layout.isDrawerOpen(GravityCompat.START)){
            drawer_layout.closeDrawer(GravityCompat.START)
        }else{
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.nav_public -> {
                startActivity(NavMyPlublications.newIntent(this, stringUser))
            }
            R.id.nav_settings -> {
                startActivity(NavSettings.newIntent(this))
            }
            R.id.nav_close_session -> {
                AlertDialog.Builder(this).setTitle(R.string.app_name as Int)
                    .setMessage(R.string.close_session_message)
                    .setCancelable(false)
                    .setPositiveButton(R.string.yes) { _, _ ->
                        Constants.closeSession(this)
                        startActivity(Intent(this, Login::class.java))
                        finish()
                    }
                    .setNegativeButton(R.string.no) { dialog, _ ->
                        dialog.dismiss()
                    }
                    .create()
                    .show()
            }
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode){
            Constants.U_USER_REQUEST ->{
                returnInformation()
                configurationBar()
            }
            Constants.C_OBJECT_REQUEST ->{
                if(resultCode == Activity.RESULT_OK){
                    (adapter as VPAdapter).nearYou.cleanMarker()
                    (adapter as VPAdapter).nearYou.fetchObjects()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        initSp()
    }

    override fun onPause() {
        super.onPause()
        if(sensorActivate)
            mSensorManager.unregisterListener(this)
    }

    private fun initSp(){
        val sharedPreferences = getSharedPreferences(Constants.name_pref, Context.MODE_PRIVATE)
        sensorActivate = sharedPreferences.getBoolean(Constants.SENSOR_ACTIVATE, false)
        if(sensorActivate)
            mSensorManager.registerListener(this, mSensors, SensorManager.SENSOR_DELAY_NORMAL)
        else{
            mSensorManager.unregisterListener(this)
            colorNormal()
        }
    }

    private fun initSensor(){
        mSensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        mSensors = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT)
    }

    private fun colorNormal(){
        theme.applyStyle(R.style.AppTheme, true)
        window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimary)
        toolbar.background = getDrawable(R.color.background_layout)
        tl_home.background = getDrawable(R.color.background_layout)
    }

    private fun colorDark(){
        theme.applyStyle(R.style.AppThemeDark, true)
        window.statusBarColor = ContextCompat.getColor(this, R.color.background_layout_dark)

        toolbar.background = getDrawable(R.color.background_layout_dark)
        tl_home.background = getDrawable(R.color.background_layout_dark)
    }

    private fun returnInformation(){
        val sharedPreferences = getSharedPreferences(Constants.name_pref, Context.MODE_PRIVATE)
        stringUser = sharedPreferences.getString("usu", "")
    }

    private fun configurationBar() {
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        val toggle = ActionBarDrawerToggle(this, drawer_layout, toolbar, 0, 0)

        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        toggle.isDrawerIndicatorEnabled = true
        navigation_view.setNavigationItemSelectedListener(this)

        val header = navigation_view.getHeaderView(0)
        val nick = header.findViewById<View>(R.id.tv_profile_name) as TextView
        val email = header.findViewById<View>(R.id.tv_profile_email) as TextView
        val nickImage: ImageView = header.findViewById<View>(R.id.iv_profile_image) as ImageView
        val pbWait = header.findViewById<View>(R.id.pb_wait) as ProgressBar

        val type = object : TypeToken<User>() {}.type
        val user = Gson().fromJson<User>(stringUser, type)

        nick.text = user.nick
        email.text = user.email
        val sb = StringBuilder()
        sb.append(Constants.pathProfile)
        sb.append(user.id)
        sb.append(Constants.typeFormatImage)
        val weakReference = WeakReference(nickImage)
        LoadImage(weakReference, this, pbWait).execute(sb.toString())

        nickImage.setOnClickListener {
            returnInformation()
            startActivityForResult(NavUpdateUser.newIntent(this, stringUser), Constants.U_USER_REQUEST )
            drawer_layout.closeDrawer(GravityCompat.START)
        }
    }

    private fun tlVpInit(){
        vp_home.adapter = adapter
        vp_home.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tl_home))
        tl_home!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabReselected(p0: TabLayout.Tab?) {}

            override fun onTabUnselected(p0: TabLayout.Tab?) {}

            override fun onTabSelected(p0: TabLayout.Tab?) {
                vp_home!!.currentItem = p0!!.position
            }
        })
    }

    override fun onFragmentInteraction(uri: Uri) { }
}

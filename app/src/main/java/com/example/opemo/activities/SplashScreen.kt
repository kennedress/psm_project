package com.example.opemo.activities

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import com.example.opemo.Constants
import com.example.opemo.R

class SplashScreen : AppCompatActivity() {

    private fun getSplashScreenDuration() = 2000L
    private val PERMISSION_INIT = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        displayChanged()
        reviewPermissions()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when(requestCode){
            PERMISSION_INIT -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    //permission from popup was granted
                    loadInformation()
                }
                else{
                    //permission from popup was denied
                    Toast.makeText(this@SplashScreen, "Permisos denegados", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun displayChanged(){
        //supportActionBar!!.hide()
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        }
    }

    private fun reviewPermissions(){
        val permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (Build.VERSION.SDK_INT >= 23) {
            var permission_denied = false

            for(permission in permissions){
                if(checkSelfPermission(permission) == PackageManager.PERMISSION_DENIED){
                    permission_denied = true
                    break
                }
            }

            if(permission_denied)
                requestPermissions(permissions, PERMISSION_INIT)
            else
                loadInformation()
        } else {
            loadInformation()
        }

    }

    private fun loadInformation(){
        val splashScreenDuration = getSplashScreenDuration()
        Handler().postDelayed({
            startActivity(Intent(this, Login::class.java))
            finish()
        },splashScreenDuration)
    }
}

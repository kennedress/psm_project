package com.example.opemo.activities

import android.Manifest
import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Build.VERSION
import android.os.Bundle
import android.provider.MediaStore
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.graphics.drawable.toBitmap
import androidx.core.widget.addTextChangedListener
import com.example.opemo.Constants
import com.example.opemo.R
import com.example.opemo.Utils
import com.example.opemo.model.User
import com.example.opemo.retrofit.WSOpemo
import com.example.opemo.service.LoadImage
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_nav_update_user.*
import java.lang.ref.WeakReference


class NavUpdateUser : AppCompatActivity() {

    lateinit var usuario: User
    var changeProfile = false
    var changeImageProfile = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nav_update_user)
        val toolbar: Toolbar = findViewById(R.id.tb_nav_update_user)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        returnInfo()
        initController()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home -> { onBackPressed() }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if(changeProfile || changeImageProfile) {
            AlertDialog.Builder(this).setTitle(R.string.app_name as Int)
                .setMessage(R.string.register_object_return_message)
                .setCancelable(false)
                .setPositiveButton(R.string.yes) { _, _ ->
                    setResult(Activity.RESULT_OK)
                    super.onBackPressed()
                }
                .setNegativeButton(R.string.no) { dialog, _ -> dialog.dismiss() }
                .create()
                .show()
        }else{
            setResult(Activity.RESULT_OK)
            super.onBackPressed()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode){
            Constants.CAMERA_REQUEST ->{
                if(resultCode == Activity.RESULT_OK){
                    val bitmap = data!!.extras!!.get("data") as Bitmap
                    iv_profile_image_detail.setImageBitmap(bitmap)
                    btn_reset.isEnabled = true
                    btn_save.isEnabled = true
                    changeImageProfile = true
                }
            }
            Constants.GALLERY_REQUEST ->{
                if(resultCode == Activity.RESULT_OK){
                    iv_profile_image_detail.setImageURI(data!!.data)
                    btn_reset.isEnabled = true
                    btn_save.isEnabled = true
                    changeImageProfile = true
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == Constants.CAMERA_PERMISSION){
            if(grantResults.isNotEmpty() || grantResults[0] != PackageManager.PERMISSION_DENIED){
                openCamera()
            }else{
                Toast.makeText(this, "Permisos Denegados", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun returnInfo(){
        try{
            intent.getStringExtra(PUT_EXTRA_USER)?.let {
                val type = object : TypeToken<User?>() {}.type
                usuario = Gson().fromJson<User>(it, type)
                loadInfo()
            }
        }catch (e: Exception){
            e.printStackTrace()
        }
    }

    private fun loadInfo(){
        tiet_nickname.setText(usuario.nick!!)
        tiet_email.setText(usuario.email!!)
        tiet_password.setText(usuario.pass!!)
        val sb = StringBuilder()
        sb.append(Constants.pathProfile)
        sb.append(usuario.id!!)
        sb.append(Constants.typeFormatImage)
        val weakReference = WeakReference(iv_profile_image_detail)
        LoadImage(weakReference, this, pb_wait).execute(sb.toString())
    }

    private fun initController(){
        tiet_nickname.addTextChangedListener { reviewEnableButton()}

        tiet_password.addTextChangedListener { reviewEnableButton() }

        tiet_email.addTextChangedListener { reviewEnableButton() }

        iv_profile_image_detail.setOnClickListener {
            AlertDialog.Builder(this)
                .setTitle(R.string.app_name)
                .setCancelable(false)
                .setItems(arrayOf("Tomar foto", "Elegir de la Galería", "Cancelar")) { dialog, which ->
                    when(which){
                        0 -> { reviewPermissionCamera() }
                        1 -> { openGallery() }
                        2 -> { dialog.dismiss() }
                        else -> { dialog.dismiss() }
                    }
                }
                .create()
                .show()
        }

        btn_close_session.setOnClickListener {
            AlertDialog.Builder(this).setTitle(R.string.app_name as Int)
                .setMessage(R.string.close_session_message)
                .setCancelable(false)
                .setPositiveButton(R.string.yes) { _, _ ->
                    Constants.closeSession(this)
                    startActivity(Intent(this, Login::class.java))
                    finish()
                }
                .setNegativeButton(R.string.no) { dialog, _ ->
                    dialog.dismiss()
                }
                .create()
                .show()
        }

        btn_reset.setOnClickListener { loadInfo() }

        btn_save.setOnClickListener {
            if(changeImageProfile){
                val b64 = Utils.convert(iv_profile_image_detail.drawable.toBitmap())
                WSOpemo.updateImageProfile(usuario.id!!, b64, Constants.typeFormatImage){ onFinish, onString, onError ->
                    if(onError != null){
                        Toast.makeText(this, onError, Toast.LENGTH_LONG).show()
                    }

                    if(onFinish != null){
                        Toast.makeText(this, onFinish.message, Toast.LENGTH_LONG).show()
                        if(!changeProfile) {
                            btn_reset.isEnabled = false
                            btn_save.isEnabled = false
                            changeProfile = false
                            changeImageProfile = false
                            onBackPressed()
                        }
                    }
                }
            }

            if(changeProfile){
                WSOpemo.updateUser(usuario.id!!, tiet_nickname.text.toString(), tiet_password.text.toString(), tiet_email.text.toString()){
                    onFinish, onString, onError ->

                    if(onError != null){
                        Toast.makeText(this, onError, Toast.LENGTH_LONG).show()
                    }

                    if(onFinish != null){
                        if(onFinish.id != null){
                            val sharedPreferences = getSharedPreferences(Constants.name_pref, Context.MODE_PRIVATE)
                            val editor = sharedPreferences.edit()
                            editor.putString("usu", onString)
                            editor.apply()
                            Toast.makeText(this, onFinish.message, Toast.LENGTH_LONG).show()
                            btn_reset.isEnabled = false
                            btn_save.isEnabled = false
                            changeProfile = false
                            changeImageProfile = false
                            onBackPressed()
                        }else{
                            Toast.makeText(this, onFinish.message, Toast.LENGTH_LONG).show()
                        }
                    }
                }
            }
        }

        btn_delete_user.setOnClickListener {
            AlertDialog.Builder(this).setTitle(R.string.app_name as Int)
                .setMessage(R.string.delete_user_message)
                .setCancelable(false)
                .setPositiveButton(R.string.yes) { _, _ ->
                    WSOpemo.deleteUser(usuario.id!!){ onFinish, _, onError ->
                        if(onError != null){
                            Toast.makeText(this, onError, Toast.LENGTH_LONG).show()
                        }

                        if(onFinish != null){
                            if(onFinish.message != null){
                                Toast.makeText(this, onFinish.message, Toast.LENGTH_LONG).show()
                                Constants.closeSession(this)
                                startActivity(Intent(this, Login::class.java))
                                finish()
                            }else{
                                Toast.makeText(this, "Ocurrió un problema, inténtelo más tarde", Toast.LENGTH_LONG).show()
                            }
                        }
                    }
                }
                .setNegativeButton(R.string.no) { dialog, _ ->
                    dialog.dismiss()
                }
                .create()
                .show()
        }
    }

    private fun reviewEnableButton(){
        changeProfile = (tiet_nickname.text.toString() != usuario.nick!! && !tiet_nickname.text.isNullOrEmpty()) ||
                (tiet_email.text.toString() != usuario.email!! && !tiet_email.text.isNullOrEmpty()) ||
                (tiet_password.text.toString() != usuario.pass!! && !tiet_password.text.isNullOrEmpty())

        if(changeProfile || changeImageProfile){
            btn_reset.isEnabled = true
            btn_save.isEnabled = true
        }else{
            btn_reset.isEnabled = false
            btn_save.isEnabled = false
        }

        if(tiet_nickname.text.isNullOrEmpty() || tiet_password.text.isNullOrEmpty() || tiet_email.text.isNullOrEmpty())
            btn_save.isEnabled = true
    }

    private fun reviewPermissionCamera(){
        if (VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Constants.camera_manifest) == PackageManager.PERMISSION_DENIED
                || checkSelfPermission(Constants.external_storage) == PackageManager.PERMISSION_DENIED) {
                requestPermissions(arrayOf(Constants.camera_manifest, Constants.external_storage), Constants.CAMERA_PERMISSION)
            } else {
                openCamera()
            }
        } else {
            openCamera()
        }
    }

    private fun openGallery(){
        Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI).also {galleryIntent ->
            startActivityForResult(Intent.createChooser(galleryIntent, "Selecciona una app de imagen"), Constants.GALLERY_REQUEST)
        }
    }

    private fun openCamera(){
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(takePictureIntent, Constants.CAMERA_REQUEST)
            }
        }
    }

    companion object{
        const val PUT_EXTRA_USER = "PUT_EXTRA_USER"

        fun newIntent(context: Context, user: String?):Intent{
            val intent =  Intent(context, NavUpdateUser::class.java)
            intent.putExtra(PUT_EXTRA_USER, user)
            return intent
        }

    }
}

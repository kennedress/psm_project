package com.example.opemo.activities

import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.example.opemo.Constants
import com.example.opemo.R
import kotlinx.android.synthetic.main.activity_nav_settings.*
import kotlinx.android.synthetic.main.activity_register.*

class NavSettings : AppCompatActivity(), SensorEventListener {

    private lateinit var mSensorManager: SensorManager
    private var mSensors: Sensor? = null
    private var sensorActivate = false

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) { }

    override fun onSensorChanged(p0: SensorEvent?) {
        // Sensor change value
        val millibarsOfPressure = p0!!.values[0].toInt()
        if (p0.sensor.type == Sensor.TYPE_LIGHT){
            when(millibarsOfPressure){
                in 0..30 ->{ colorDark() }
                in 31..100-> { colorNormal() }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nav_settings)

        val toolbar: Toolbar = findViewById(R.id.tb_nav_settings)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        initSp()
        initSensor()
        initController()
    }

    override fun onResume() {
        super.onResume()
        if(sensorActivate)
            mSensorManager.registerListener(this, mSensors, SensorManager.SENSOR_DELAY_NORMAL)
    }

    override fun onPause() {
        super.onPause()
        if(sensorActivate)
            mSensorManager.unregisterListener(this)
    }

    private fun initSp(){
        val sharedPreferences = getSharedPreferences(Constants.name_pref, Context.MODE_PRIVATE)
        sensorActivate = sharedPreferences.getBoolean(Constants.SENSOR_ACTIVATE, false)
        if(sensorActivate)
            sc_intelligent_color.isChecked = true
    }

    private fun initSensor(){
        mSensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        mSensors = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT)
    }

    private fun initController(){
        sc_intelligent_color.setOnCheckedChangeListener { _, isChecked ->
            val sharedPreferences = getSharedPreferences(Constants.name_pref, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            if(isChecked){
                editor.putBoolean(Constants.SENSOR_ACTIVATE, true)
                mSensorManager.registerListener(this, mSensors, SensorManager.SENSOR_DELAY_NORMAL)
            }else{
                editor.putBoolean(Constants.SENSOR_ACTIVATE, false)
                colorNormal()
                mSensorManager.unregisterListener(this)
            }
            editor.apply()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home -> { finish() }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun colorNormal(){
        tb_nav_settings.background = getDrawable(R.color.background_layout)
        theme.applyStyle(R.style.AppTheme, true)
        window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimary)
    }

    private fun colorDark(){
        tb_nav_settings.background = getDrawable(R.color.background_layout_dark)
        theme.applyStyle(R.style.AppThemeDark, true)
        window.statusBarColor = ContextCompat.getColor(this, R.color.background_layout_dark)
    }

    companion object{
        fun newIntent(context: Context): Intent {
            return Intent(context, NavSettings::class.java)
        }

    }
}

package com.example.opemo.activities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.opemo.R
import com.example.opemo.adapter.RVCommenterAdapter
import com.example.opemo.model.LocalImageObject
import com.example.opemo.model.Message
import com.example.opemo.retrofit.WSOpemo
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_c_r_u_d_message.*
import java.text.SimpleDateFormat
import java.util.*

class CRUDMessage : AppCompatActivity() {

    var idUser: String? = null
    var idObject: String? = null
    var listComentersString: String? = null
    var listComenters: List<Message?>? = null
    var mAdapter: RVCommenterAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_c_r_u_d_message)
        returnInformation()
        initController()
    }

    private fun returnInformation(){
        intent.getStringExtra(PUT_EXTRA_ID_USER)?.let { idUser = it }

        intent.getStringExtra(PUT_EXTRA_ID_OBJECT)?.let { idObject = it }

        intent.getStringExtra(PUT_EXTRA_LIST_COMMENTERS)?.let {
            val type = object : TypeToken<List<Message?>?>() {}.type
            listComenters = Gson().fromJson<List<Message?>?>(it, type)
            listComentersString = it

            mAdapter = RVCommenterAdapter(this, listComenters!!, idUser!!)
            rv_comentarios.adapter = mAdapter
        }
    }

    private fun initController(){
        ib_send_commenter.setOnClickListener {
            if(et_commenter.text.toString().isEmpty()){
                Toast.makeText(this, "Debe escribir un comentario", Toast.LENGTH_LONG).show()
            }else{
                val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
                WSOpemo.createMessage(idUser!!, idObject!!, et_commenter.text.toString(), sdf.format(Date()).toString()){
                    onFinish: Message?, onError: String? ->

                    if(onError != null){ Toast.makeText(this, onError, Toast.LENGTH_LONG).show() }

                    if(onFinish != null) {
                        updateList()
                    }
                }
            }
        }
    }

    private fun updateList(){
        WSOpemo.readMessage(idObject!!){ onFinish, onString, onError ->
            if(onError != null){ Toast.makeText(this, onError, Toast.LENGTH_LONG).show() }

            if(onString != null){
                listComenters = onFinish
                listComentersString = onString
                mAdapter?.notifyDataSetChanged()
            }
        }
    }

    companion object{
        const val PUT_EXTRA_ID_USER = "PUT_EXTRA_ID_USER"
        const val PUT_EXTRA_ID_OBJECT = "PUT_EXTRA_ID_OBJECT"
        const val PUT_EXTRA_LIST_COMMENTERS = "PUT_EXTRA_LIST_COMMENTERS"

        fun newIntent(context: Context, idUser: String?, idObject: String?, listCommenters: String?): Intent {
            val intent =  Intent(context, CRUDMessage::class.java)
            intent.putExtra(PUT_EXTRA_ID_USER, idUser)
            intent.putExtra(PUT_EXTRA_ID_OBJECT, idObject)
            intent.putExtra(PUT_EXTRA_LIST_COMMENTERS, listCommenters)
            return intent
        }
    }
}
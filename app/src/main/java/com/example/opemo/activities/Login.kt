package com.example.opemo.activities

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.example.opemo.R
import com.example.opemo.retrofit.WSOpemo
import kotlinx.android.synthetic.main.activity_login.*

class Login : AppCompatActivity() {

    val name_pref = "usuario"
    lateinit var sharedPreferences : SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        sp()
        controllerInit()
    }


    private fun displayChanged(){
        //supportActionBar!!.hide()
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
    }

    private fun sp(){
        sharedPreferences = getSharedPreferences(name_pref, Context.MODE_PRIVATE)
        val usu = sharedPreferences.getString("usu", "")

        if(!usu.equals("")){
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun controllerInit(){
        tv_Register!!.setOnClickListener { openRegister() }
        btn_login!!.setOnClickListener { openHome() }
    }

    private fun openRegister(){
        val intent = Intent(this, RegisterUser::class.java)
        startActivity(intent)
    }

    private fun openHome(){
        val usuario:String = et_user!!.text.toString()
        val contra:String = et_pass!!.text.toString()

        if(usuario != "" && contra != ""){
            WSOpemo.login(usuario, contra){ onFinish, onString, onError ->
                if(onError != null){
                    Toast.makeText(this, onError, Toast.LENGTH_LONG).show()

                    YoYo.with(Techniques.Tada)
                        .duration(700)
                        .repeat(1)
                        .playOn(EditTxt_UserLogin)

                    YoYo.with(Techniques.Tada)
                        .duration(700)
                        .repeat(1)
                        .playOn(EditTxt_PasswordLogin)
                }

                if(onFinish != null){
                    if(onFinish.id != null){
                        val editor = sharedPreferences.edit()
                        editor.putString("usu", onString)
                        editor.apply()
                        startActivity(Intent(this, MainActivity::class.java))
                        finish()
                    }else{
                        Toast.makeText(this, onFinish.message, Toast.LENGTH_LONG).show()

                        YoYo.with(Techniques.Tada)
                            .duration(700)
                            .repeat(1)
                            .playOn(EditTxt_UserLogin)

                        YoYo.with(Techniques.Tada)
                            .duration(700)
                            .repeat(1)
                            .playOn(EditTxt_PasswordLogin)
                    }
                }
            }
        }else{
            Toast.makeText(this, "Todos los campos deben ser llenados", Toast.LENGTH_SHORT).show()
            if(usuario == "") {
                YoYo.with(Techniques.Tada)
                    .duration(700)
                    .repeat(1)
                    .playOn(EditTxt_UserLogin)
            }

            if(contra == "") {
                YoYo.with(Techniques.Tada)
                    .duration(700)
                    .repeat(1)
                    .playOn(EditTxt_PasswordLogin)
            }
        }
    }
}

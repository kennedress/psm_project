package com.example.opemo.activities

import android.Manifest
import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.example.opemo.Constants
import com.example.opemo.R
import com.example.opemo.Utils
import com.example.opemo.adapter.RVImageObjectAdapter
import com.example.opemo.model.LocalImageObject
import com.example.opemo.model.Object
import com.example.opemo.model.User
import com.example.opemo.retrofit.WSOpemo
import com.example.opemo.service.CreateObject
import com.example.opemo.service.IntentCreateObject
import com.example.opemo.sqlite.DbImageObjectLocal
import com.example.opemo.sqlite.DbObjectLocal
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_nav_update_user.*
import kotlinx.android.synthetic.main.activity_register_object.*
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class RegisterObject : AppCompatActivity() {

    lateinit var user: User
    val listImage: MutableList<LocalImageObject> = arrayListOf()
    var mAdapter: RVImageObjectAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_object)
        val toolbar: Toolbar = findViewById(R.id.tb_register_object)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        returnInformation()
        initController()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home -> {
                setResult(Activity.RESULT_CANCELED)
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode){
            Constants.CAMERA_REQUEST ->{
                if(resultCode == Activity.RESULT_OK){
                    val bitmap = data!!.extras!!.get("data") as Bitmap
                    val sdf = SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
                    val b64 = Utils.convert(bitmap)

                    listImage.add(LocalImageObject(null, sdf.format(Date()).toString(), b64, bitmap, "jpeg"))
                    mAdapter!!.notifyDataSetChanged()
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == Constants.CAMERA_PERMISSION){
            if(grantResults.isNotEmpty() || grantResults[0] != PackageManager.PERMISSION_DENIED){
                openCamera()
            }else{
                Toast.makeText(this, "Permisos Denegados", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    private fun returnInformation(){
        try{
            intent.getStringExtra(PUT_EXTRA_USER)?.let {
                Gson().fromJson(it, User::class.java)?.let { User ->
                    this@RegisterObject.user = User
                }
            }
        }catch (e: Exception){

        }
    }

    private fun initController(){
        rv_photo_object.setHasFixedSize(true)
        mAdapter = RVImageObjectAdapter(this, listImage, false)
        rv_photo_object.adapter = mAdapter

        btn_save_object.setOnClickListener { addObject() }

        btn_take_photo.setOnClickListener { reviewPermissionCamera() }
    }

    private fun addObject(){
        val id:String = user.id!!
        val title:String = tiet_title!!.text.toString()
        val description:String = tiet_description!!.text.toString()
        val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        if(title != "" && description != "" && listImage.size > 0){
            if(checkLocation(this@RegisterObject)) {
                if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {

                    return
                }
                fusedLocationClient.lastLocation.addOnSuccessListener { location ->
                    val lat = location.latitude
                    val lng = location.longitude

                    val objetoCreado = Object(null, id, title, description, sdf.format(Date()).toString(), null, lat.toString(), lng.toString(), null)
                    // Guardamos en una base de datos
                    val db = DbObjectLocal(this)
                    db.addLocalImageObject(objetoCreado)
                    val dbImage = DbImageObjectLocal(this)
                    listImage.forEach { dbImage.addLocalImageObject(it) }
                    // Llamamos el servicio para subir el registro y las fotos

                    //val intent = Intent(this, CreateObject::class.java)
                    val intent = Intent(this, IntentCreateObject::class.java)
                    startService(intent)
                    setResult(Activity.RESULT_OK)
                    onBackPressed()
                }
            }else{
                Toast.makeText(this, "Hay problemas con tu ubicación inténtalo más tarde", Toast.LENGTH_SHORT).show()
            }
        }else{
            Toast.makeText(this, "Todos los campos deben ser llenados", Toast.LENGTH_SHORT).show()
            if(title == ""){
                YoYo.with(Techniques.Tada)
                    .duration(700)
                    .repeat(1)
                    .playOn(til_title)
            }

            if(description == ""){
                YoYo.with(Techniques.Tada)
                    .duration(700)
                    .repeat(1)
                    .playOn(til_description)
            }

        }
    }

    private fun reviewPermissionCamera(){
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Constants.camera_manifest) == PackageManager.PERMISSION_DENIED
                || checkSelfPermission(Constants.external_storage) == PackageManager.PERMISSION_DENIED) {
                requestPermissions(arrayOf(Constants.camera_manifest, Constants.external_storage), Constants.CAMERA_PERMISSION)
            } else {
                openCamera()
            }
        } else {
            openCamera()
        }
    }

    private fun openCamera(){
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(takePictureIntent, Constants.CAMERA_REQUEST)
            }
        }
    }

    private fun checkLocation(context: Context) : Boolean {
        if (!isLocationEnabled(context))
            showAlert(context)
        return isLocationEnabled(context)
    }


    private fun isLocationEnabled(context: Context) : Boolean{
        var locationMode = 0
        val locationProviders: String?

        try{
            locationMode = Settings.Secure.getInt(context.contentResolver, Settings.Secure.LOCATION_MODE)
            return locationMode != Settings.Secure.LOCATION_MODE_OFF
        }catch (e: Settings.SettingNotFoundException){
            e.printStackTrace()
        }

        return locationMode != Settings.Secure.LOCATION_MODE_OFF

    }

    private fun showAlert(context: Context){
        AlertDialog.Builder(context)
            .setTitle("Enable Location")
            .setMessage("Your location is disabled. \nPlis activate your location")
            .setPositiveButton("Settings location") { _, _ ->
                context.startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            }
            .setNegativeButton("Cancel"){ _, _ -> }
            .show()
    }

    companion object{
        const val PUT_EXTRA_USER = "PUT_EXTRA_USER"
        const val PUT_EXTRA_LAT = "PUT_EXTRA_LAT"
        const val PUT_EXTRA_LNG = "PUT_EXTRA_LNG"

        fun newIntent(context: Context, user: String?, lat: Double, lng: Double): Intent {
            val intent =  Intent(context, RegisterObject::class.java)
            intent.putExtra(PUT_EXTRA_USER, user)
            return intent
        }
    }
}

package com.example.opemo.activities

import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.example.opemo.Constants
import com.example.opemo.R
import com.example.opemo.adapter.RVImageObjectAdapter
import com.example.opemo.adapter.RVMyPublicationsAdapter
import com.example.opemo.model.User
import com.example.opemo.retrofit.WSOpemo
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_nav_my_plublications.*

class NavMyPlublications : AppCompatActivity(),
    SensorEventListener {

    lateinit var usuario: User
    var mAdapter: RVMyPublicationsAdapter? = null

    private lateinit var mSensorManager: SensorManager
    private var mSensors: Sensor? = null
    private var sensorActivate = false

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) { }

    override fun onSensorChanged(p0: SensorEvent?) {
        // Sensor change value
        val millibarsOfPressure = p0!!.values[0].toInt()
        if (p0.sensor.type == Sensor.TYPE_LIGHT){
            when(millibarsOfPressure){
                in 0..30 ->{ colorDark() }
                in 31..100-> { colorNormal() }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nav_my_plublications)
        val toolbar: Toolbar = findViewById(R.id.tb_nav_publication)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        returnInfo()
        initSensor()
        initSp()
    }

    override fun onResume() {
        super.onResume()
        initSp()
    }

    override fun onPause() {
        super.onPause()
        if(sensorActivate)
            mSensorManager.unregisterListener(this)
    }

    private fun initSp(){
        val sharedPreferences = getSharedPreferences(Constants.name_pref, Context.MODE_PRIVATE)
        sensorActivate = sharedPreferences.getBoolean(Constants.SENSOR_ACTIVATE, false)
        if(sensorActivate)
            mSensorManager.registerListener(this, mSensors, SensorManager.SENSOR_DELAY_NORMAL)
        else{
            mSensorManager.unregisterListener(this)
            colorNormal()
        }
    }

    private fun initSensor(){
        mSensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        mSensors = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT)
    }

    private fun colorNormal(){
        theme.applyStyle(R.style.AppTheme, true)
        window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimary)

        tb_nav_publication.background = getDrawable(R.color.background_layout)
    }

    private fun colorDark(){
        theme.applyStyle(R.style.AppThemeDark, true)
        window.statusBarColor = ContextCompat.getColor(this, R.color.background_layout_dark)

        tb_nav_publication.background = getDrawable(R.color.background_layout_dark)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home -> { finish() }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun returnInfo(){
        try{
            intent.getStringExtra(NavUpdateUser.PUT_EXTRA_USER)?.let {
                val type = object : TypeToken<User?>() {}.type
                usuario = Gson().fromJson(it, type)

                WSOpemo.readObjectUser(usuario.id!!){ onFinish, onString, onError ->
                    if(onError != null){
                        if(onError == "nada")
                            Toast.makeText(this, "No has publicado nada :c", Toast.LENGTH_LONG).show()
                        else
                            Toast.makeText(this, onError, Toast.LENGTH_LONG).show()
                        pb_wait.visibility = View.GONE
                    }

                    if(onString != null){
                        if(onFinish?.get(0)?.id != null){
                            rv_publications.setHasFixedSize(true)
                            mAdapter = RVMyPublicationsAdapter(this, onFinish, it)
                            rv_publications.adapter = mAdapter
                            pb_wait.visibility = View.GONE
                        }
                    }
                }
            }
        }catch (e: Exception){
            e.printStackTrace()
        }
    }


    companion object{
        const val PUT_EXTRA_USER = "PUT_EXTRA_USER"

        fun newIntent(context: Context, user: String?): Intent {
            val intent =  Intent(context, NavMyPlublications::class.java)
            intent.putExtra(PUT_EXTRA_USER, user)
            return intent
        }

    }
}
